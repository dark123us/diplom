from sqlalchemy import Column, Index, Integer, Text, String, Date, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import  scoped_session, sessionmaker, relationship
from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class WorkerDolgORM(Base):
    __tablename__ = 'workerdolg'
    id = Column(Integer, primary_key=True)
    name = Column(String)

class WorkshopORM(Base):
    __tablename__ = 'workshop'
    id = Column(Integer, primary_key=True)
    name = Column(String)

class WorkerORM(Base):
    __tablename__ = 'worker'
    id = Column(Integer, primary_key=True)
    tabel = Column(Integer)
    name = Column(String)
    address = Column(String)
    datework = Column(Date)
    workerdolg     = relationship('WorkerDolgORM')
    workerdolgid   = Column(Integer, ForeignKey('workerdolg.id'))
    workshop     = relationship('WorkshopORM')
    workshopid   = Column(Integer, ForeignKey('workshop.id'))
    knowledge_check = Column(Date)
    knowledge_check_next = Column(Date)
    medic = Column(Date)
    medic_next = Column(Date)

class KnowledgeORM(Base):
    __tablename__ = 'knowledge'
    id = Column(Integer, primary_key=True)
    knowledgedate = Column(Date)
    worker     = relationship('WorkerORM')
    workerid   = Column(Integer, ForeignKey('worker.id'))
    answer     = relationship('AnswerORM')
    answerid   = Column(Integer, ForeignKey('answer.id'))

class QuestionORM(Base):
    __tablename__ = 'question'
    id = Column(Integer, primary_key=True)
    text = Column(Text)

class AnswerORM(Base):
    __tablename__ = 'answer'
    id = Column(Integer, primary_key=True)
    question     = relationship('QuestionORM')
    questionid   = Column(Integer, ForeignKey('question.id'))
    text = Column(String)
    right = Column(Boolean)



